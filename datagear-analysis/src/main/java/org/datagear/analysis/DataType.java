/*
 * Copyright (c) 2018 datagear.tech. All Rights Reserved.
 */

/**
 * 
 */

package org.datagear.analysis;

/**
 * 数据类型。
 * 
 * @author datagear@163.com
 *
 */
public enum DataType
{
	/** 字符串 */
	STRING,

	/** 布尔值 */
	BOOLEAN,

	/** 整数 */
	INTEGER,

	/** 小数 */
	DECIMAL,

	/** 日期 */
	DATE,

	/** 时间 */
	TIME,

	/** 时间戳 */
	TIMESTAMP
}
