/*
 * Copyright (c) 2018 datagear.tech. All Rights Reserved.
 */

/**
 * 
 */
package org.datagear.util;

/**
 * 全局常量。
 * 
 * @author datagear@163.com
 *
 */
public final class Global
{
	private Global()
	{
		throw new UnsupportedOperationException();
	}

	/** 英文产品名称 */
	public static final String PRODUCT_NAME_EN = "DataGear";
}
